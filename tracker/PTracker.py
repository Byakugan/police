import cv2
import numpy as np
from scipy.optimize import linear_sum_assignment

def cv2_bbox_to_IOU(a):
  return (a[0],a[1],a[0]+a[2],a[1]+a[3])
def cv2_bbox_to_tf(a,x,y):
  return np.array([
    a[1]/y,
    a[0]/x,
    min((a[1]+a[3])/y,1),
    min((a[0]+a[2])/x,1)
  ])
rmv_traker=20
trk_id=0
num_avg=15
def getNewId():
  global trk_id
  trk_id+=1
  return trk_id
class PersonTracker:
  def __init__(self, id,frame,bbox, **kwargs):
    self.id = id
    self.bbox=bbox
    self.tracker = cv2.TrackerCSRT_create()
    self.fails=0
    self.new=True
    #stats stuf
    self.stats=kwargs.get('stats', [None]*num_avg)
    self.stats_pos=kwargs.get('stats_pos', num_avg-1)
    self.sum=kwargs.get('sum', 0)
    ####
    self.ok = self.tracker.init(frame,bbox)
    if not self.ok:
      print("tracker init not ok!")
      self.fails+=rmv_traker+1
  def update(self,frame):
    self.ok, self.bbox = self.tracker.update(frame)
    if not self.ok:
      self.fails+=1
      print("tracker update not ok!")
    else:
      self.fails=max(0,self.fails-0.1)
  def update_stats(self,clas_scor,person_scor):
    val=2*(clas_scor-0.5)*person_scor/len(self.stats)
    if self.stats[self.stats_pos] is None:
      self.stats[self.stats_pos]=val
      self.sum+=val
    else:
      self.sum=self.sum-self.stats[self.stats_pos]+val
      self.stats[self.stats_pos]=val
    self.stats_pos=(self.stats_pos-1)%len(self.stats)
    
  def get_class(self):
    if abs(self.sum)<0.45:
      return abs(self.sum),3
    return abs(self.sum),91 if self.sum<0 else 1
    
  def destroy(self):
    return self.fails>rmv_traker


class PersonDetector:
  
  def __init__(self,score,clas_score,bbox,tf_bbox):
    self.tot_scor=0
    self.clas = 3
    self.score = score
    self.clas_score=clas_score
    self.bbox=bbox
    self.id=-1
    self.tf_bbox=tf_bbox
  
def box_iou2(a, b):
    '''
    Helper funciton to calculate the ratio between intersection and the union of
    two boxes a and b
    a[0], a[1], a[2], a[3] <-> left, up, right, bottom
    '''
    a=cv2_bbox_to_IOU(a)
    b=cv2_bbox_to_IOU(b)
    
    w_intsec = np.maximum (0, (np.minimum(a[2], b[2]) - np.maximum(a[0], b[0])))
    h_intsec = np.maximum (0, (np.minimum(a[3], b[3]) - np.maximum(a[1], b[1])))
    s_intsec = w_intsec * h_intsec
    s_a = (a[2] - a[0])*(a[3] - a[1])
    s_b = (b[2] - b[0])*(b[3] - b[1])
  
    return float(s_intsec)/(s_a + s_b -s_intsec)
def assign_detections_to_trackers(trackers, detections,frame, iou_thrd = 0.05):
    for trk in trackers:
      trk.update(frame)
      trk.new=False
    trackers = [tr for tr in trackers if not tr.destroy()]

    if len(detections)<1:
      for trk in trackers:
        trk.fails+=1.1
      trackers = [tr for tr in trackers if not tr.destroy()]
      return [],trackers
    
    #remove over detections
    i=0
    while i<len(detections):
      j=i+1
      while j<len(detections):
        iou=box_iou2(detections[i].bbox,detections[j].bbox)
        if iou>0.3:
          del detections[j]
          j-=1
        j+=1
      i+=1


    matches = []
    unmatched_trackers, unmatched_detections = [], []

    
    if len(trackers)>0:          
      IOU_mat= np.zeros((len(trackers),len(detections)),dtype=np.float32)
      for t,trk in enumerate(trackers):
          #trk = convert_to_cv2bbox(trk) 
          for d,det in enumerate(detections):          
              IOU_mat[t,d] = box_iou2(trk.bbox,det.bbox) 
          
      
      # Produces matches       
      # Solve the maximizing the sum of IOU assignment problem using the
      # Hungarian algorithm (also known as Munkres algorithm)
      matched_idx = linear_sum_assignment(-IOU_mat)        
      
      for t,trk in enumerate(trackers):
          if(t not in matched_idx[0]):
              unmatched_trackers.append(t)

      for d, det in enumerate(detections):
          if(d not in matched_idx[1]):
              unmatched_detections.append(d)
      
      
      # For creating trackers we consider any detection with an 
      # overlap less than iou_thrd to signifiy the existence of 
      # an untracked object
      
      for i in range(len(matched_idx[0])):
        r=matched_idx[0][i]
        c=matched_idx[1][i]
        if(IOU_mat[r,c]<iou_thrd):
            unmatched_trackers.append(r)
            unmatched_detections.append(c)
        else:
            matches.append([c,r])
     
    else:
      unmatched_detections=range(len(detections))

    
    for det in unmatched_detections:
      tr=PersonTracker(getNewId(),frame,detections[det].bbox)
      trackers+=[tr]
      matches+=[[det,len(trackers)-1]]
    
    
    detections,trackers=transfer_maches(detections,trackers,matches,frame)

    #update faild trakers and delete
    for tr in unmatched_trackers:
      trackers[tr].fails+=1.1
    trackers = [tr for tr in trackers if not tr.destroy()]


    return detections,trackers
    
def transfer_maches(detections,trackers,matches,frame):
  for i in matches:
    det=detections[i[0]]
    trk=trackers[i[1]]
    trk.update_stats(det.clas_score,det.score)
    if not trackers[i[1]].new:
      trackers[i[1]]=PersonTracker(trk.id,frame,det.bbox,stats=trk.stats,stats_pos=trk.stats_pos,sum=trk.sum)    
    
    score,clas=trk.get_class()
    det.id=trk.id
    det.clas=clas
    det.tot_scor=score*0.92

  return detections,trackers


  
    